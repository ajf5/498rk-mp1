/* Your JS here. */
console.log('Hello World!')

var currentlySelected = null;
var currentSlide = 0;
var nav = document.getElementById("navbar");
var navBoxStart = nav.getBoundingClientRect().bottom;

var navClick = function(){
    let sectionBox = this.getBoundingClientRect();
    let navBox = nav.getBoundingClientRect();
    let desiredSpot = sectionBox.top - (navBox.bottom - window.pageYOffset);
    window.scrollTo({
      top: desiredSpot,
      behavior: 'smooth'
    });
}

for (navChild of nav.children)
{
    let id = navChild.id.substring(4);
    let section = document.getElementById(id);
    navChild.addEventListener('click', function(e) { navClick.call(section); });
}

var getSlideNumber = (function(dir, total) {
    var slide = 0;
    var slides = total;
    return function () {
        slide += dir;
        if (slide < 0) { slide = slides-1; }
        if (slide == slides) { slide = 0; }
        return slide;
    }
});

var performClick = function(getNext){
    let tempImg = document.getElementById("img" + currentSlide.toString());
    currentSlide = getNext();
    let nextImg = document.getElementById("img" + currentSlide.toString());
    tempImg.classList.add("hidden");
    nextImg.classList.remove("hidden");
    let arrow = this.children[1];
    arrow.style.fill = "white";
    let clear = () => {arrow.style.fill = "none";};
    setTimeout(clear, 400);
};

class Button {
    constructor(elementName, slideFunc){
        this.name = elementName;
        this.element = document.getElementById(elementName);
        let x = () => { performClick.call(this.element, slideFunc); };
        this.element.addEventListener('click', function(e) { x(); });
    }
}

//they go in the wrong order bewcause of html/css issue
var nextButton = new Button("slide-next", getSlideNumber(1,4));
var prevButton = new Button("slide-prev", getSlideNumber(-1,4));

document.addEventListener('scroll', function(e) {
  var nav_offset = nav.getBoundingClientRect();
  var nav_bottom = nav_offset.bottom;
  var topElem;
  var topPos = 1000000;
  for (element of nav.children)
  {
      let id = element.id.substring(4);
      let section = document.getElementById(id);
      let offset = section.getBoundingClientRect();
      let top = offset.top;
      let diff = nav_bottom - top;
      if (diff >= 0)
      {
          if (diff < topPos)
          {
              topPos = diff;
              topElem = element;
          }
      }
  }
  if (currentlySelected != topElem && currentlySelected != null){
      currentlySelected.classList.remove("navbar-selected");
  }
  currentlySelected = topElem;
  currentlySelected.classList.add("navbar-selected");
});

var hasResized = false;
var oldScrollValue = 0;

//https://stackoverflow.com/questions/26922488/access-to-index-of-element-in-array-of-elements-with-common-classname
var arrayConverter = function(obj) {
  return Array.prototype.map.call(obj, (element) => {return element;})
};

document.addEventListener('scroll', function(e) {
    let obj = document.getElementsByClassName("nav-bar-text");
    let missingItem = [document.getElementById("nav-main-header")];
    let arr = [...missingItem, ...arrayConverter(obj)];
    let yOffset = window.pageYOffset;
    if (yOffset > 250 && !hasResized && yOffset > oldScrollValue){
        for (var i = 0; i < arr.length; i++) {
            arr[i].classList.remove("navbar-fullsize");
            arr[i].classList.add("navbar-shrunk");
        }
        hasResized = true;
    }
    else if (yOffset < 250 && hasResized && oldScrollValue > yOffset) {
        for (var i = 0; i < arr.length; i++) {
            arr[i].classList.add("navbar-fullsize");
            arr[i].classList.remove("navbar-shrunk");
        }
        hasResized = false;
    }
    oldScrollValue = yOffset;
});

//https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_modal
var modal = document.getElementById("modal-vader");
var modalTrigger = document.getElementById("modal-trigger");
var modalClose = document.getElementById("modal-close");
modalTrigger.addEventListener('click', function(e) {modal.style.display = "block";});
modalClose.addEventListener('click', function(e) {modal.style.display = "none";});
modal.addEventListener('click', function(e) {modal.style.display = "none";});
